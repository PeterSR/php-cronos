Cronos
======

PHP Cron job library


Usage
-----

Update crontab manually by inserting this line

`*/10 * * * * /usr/bin/php /path/to/website/cronjob.php`

to run something every 10 minutes. A future release of Cronos might be able to do this for you.

And in `cronjob.php`, do the following:

Create a job

`$job = Cronos::job("myfirstjob")->at("*/10 * * * *")->perform("myfirstjobscript.php")->register();`

and run Cronos

`Cronos::run();`

That's it! Now, when `cronjob.php` is loaded by the crontab at 0:10, the script infers that the job to execute is the job that we gave it. It will load the script `myfirstjobscript.php` to perform the task.
