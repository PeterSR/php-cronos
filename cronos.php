<?php

/**
 *   ____
 *  / ___|_ __ ___  _ __   ___  ___ 
 * | |   | '__/ _ \| '_ \ / _ \/ __|
 * | |___| | | (_) | | | | (_) \__ \
 *  \____|_|  \___/|_| |_|\___/|___/
 * 
 * Cronos | Cron job handler | 2014
 * 
 * @version 0.1.5
 * @author Peter Severin Rasmussen <github@petersr.com>
 * 
 * Cronos is licensed under the MIT license. 
 */

/**
 * Cronos
 * Main class
 */
class Cronos {

    private static $inputKey;
    private static $key;
    private static $jobpath;
    private static $current;
    private static $jobs = array();
    private static $db;
    private static $unnamedJobsCounter = 0;

    /**
     * The standard way to execute cron jobs
     * Based on the current time, the function infers what jobs to execute 
     * based on their specified cron expression. 
     * If no such expression is supplied, the job can only be executed with Cronos::execute(jobname);
     */
    public static function run() {
        $now = time();

        // Find job to execute
        foreach (self::$jobs as $job) {
            $jobexpression = $job->getCronExp();

            if (CronosExpression::match($jobexpression, $now)) {
                self::execute($job->name());
            }
        }
    }

    /**
     * Execute a job by supplying a jobname
     * 
     * @param string $jobname
     */
    public static function execute($jobname) {
        self::checkKey();

        if (!isset(self::$db)) {
            Cronos::report("No database");
        }

        if (!isset(self::$jobs[$jobname])) {
            Cronos::report("No such job '" . $jobname . "'");
        }

        $running = self::$db->findRunning($jobname);

        if (count($running) > 0) {
            echo "Job " . $jobname . " already running";
            return;
        }

        self::$current = $jobname;
        $job = self::$jobs[$jobname];
        $task = $job->getTask();

        $jobhandler = $job->handler();

        $running_time = -microtime(true);

        // Execute job
        if (is_string($task)) {
            if (!isset(self::$jobpath)) {
                Cronos::report("Missing job path");
            }

            Cronos::environment($jobhandler, rtrim(self::$jobpath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $task);
        } else if (is_callable($task)) {
            $task($jobhandler);
        }

        $running_time += microtime(true);

        $jobhandler->close($running_time);

        self::$current = null;
    }

    /**
     * Store a job in the job array
     * 
     * @param CronosJob $job
     */
    public static function register(CronosJob $job) {
        if (is_object($job) && ($job instanceof CronosJob)) { // Old check
            $name = $job->name();
            if (!isset(self::$jobs[$name])) {
                self::$jobs[$name] = $job;
            } else {
                Cronos::report("Job with name '" . $name . "' already registered");
            }
        } else {
            Cronos::report($job . " not a Cronos Job");
        }
    }

    /**
     * Register more than one job
     * Can be either an array or list of parameters
     * 
     * @param mixed $list
     */
    public static function registerAll($list) {
        if ($list !== null && is_array($list)) {
            foreach ($list as $job) {
                self::register($job);
            }
        } else {
            foreach (func_get_args() as $job) {
                self::register($job);
            }
        }
    }

    /**
     * Shorthand to create a new job instance
     * It also takes care of unnamed jobs and sets the database
     * 
     * @param string $name
     * @return \CronosJob
     */
    public static function job($name = null) {
        if ($name === null) {
            self::$unnamedJobsCounter++;
            $name = "Unnamed" . self::$unnamedJobsCounter;
        }

        $job = new CronosJob($name);
        $job->setDB(self::$db);
        return $job;
    }

    /**
     * Set up Cronos
     * 
     * The method takes an array of settings.
     * Configurable things:
     *  - key: Set a key such that only only scripts with key can execute job
     *  - jobpath: Set the path where the different jobs will be fetched from
     *  - database: Database configuration, see CronosDB for details
     * 
     * @param array $config
     */
    public static function setup($config) {
        $bt = debug_backtrace();

        if (isset($config['key'])) {
            self::$key = $config['key'];
        }

        if (isset($config['jobpath'])) {
            self::$jobpath = $config['jobpath'];
        } else if (isset($bt[0]['file'])) {
            self::$jobpath = dirname($bt[0]['file']);
        }

        if (isset($config['database'])) {
            self::$db = new CronosDB($config['database']);
        }
    }

    /**
     * Apply an input key and check if it is valid
     * 
     * @param string $key
     */
    public static function insertKey($key) {
        self::$inputKey = $key;
        self::checkKey();
    }

    /**
     * Checks if the input key matches the stored key from the setup
     * 
     * @return boolean Key is correct or not
     */
    public static function checkKey() {
        if (isset(self::$key) && (self::$inputKey !== self::$key)) {
            Cronos::report("Incorrect key");
            return false;
        } else {
            return true;
        }
    }

    /**
     * A little helper function to safely embed a job file
     * into an empty scope only supplied with the jobhandler
     * 
     * @param CronosJobHandler $jobhandler The given job handler
     * @param string $file Path to script file
     */
    private static function environment($jobhandler, $file) {
        include($file);
    }

    /**
     * Report an error and if necessary halt the progarm
     * 
     * @param string $msg The error message
     * @param boolean $die Halt the program?
     */
    public static function report($msg, $die = true) {
        $bt = debug_backtrace();
        $classprefix = (isset($bt[1]['class']) ? $bt[1]['class'] . ": " : "");
        echo $classprefix . $msg;
        if ($die) {
            die();
        }
    }

}

/**
 * CronosDB
 * The connection with the database using PDO
 */
class CronosDB {

    private $driver;
    private $table_prefix;
    private $tables = array();
    private $running_timeout = 600; // 10 minutes

    /**
     * Configure and connect to the database
     * 
     * Configuration:
     *  - dbtype: The database type - mysql, mssql etc. [Default: mysql]
     *  - host: The host of the database, usually localhost [Default: localhost]
     *  - name: The database name
     *  - username: The username that will connect to the database
     *  - password: The corresponding password to the user
     *  - custom: A custom string to connect to the DB with. If set, all above will be ignored
     *  - prefix: The prefix in front of the table names [Default: cronos_]
     * 
     * @param array $config
     */
    public function __construct($config) {
        if (!isset($config['dbtype'])) {
            $config['dbtype'] = "mysql";
        }
        if (!isset($config['host'])) {
            $config['host'] = "localhost";
        }
        if (!isset($config['prefix'])) {
            $config['prefix'] = "cronos_";
        }

        if (isset($config['running_timeout'])) {
            $this->running_timeout = intval($config['running_timeout']);
        }

        $this->table_prefix = $config['prefix'];
        $this->setTableName();

        try {
            if (isset($config['custom'])) {
                $this->driver = new PDO($config['custom']);
            } else {
                $this->driver = new PDO($config['dbtype'] . ":host=" . $config['host'] . ";dbname=" . $config['name'], $config['username'], $config['password']);
            }

            $this->driver->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->createTables();
        } catch (PDOException $e) {
            Cronos::report($e->getMessage());
        }
    }

    /**
     * Find running jobs
     * Optionally with a jobname to restrict the search
     * 
     * @param string $jobname
     * @return array A list of running jobs
     */
    function findRunning($jobname = null) {
        $running = array();

        $statement = $this->driver->prepare("select * from " . $this->table("job") . " where running = :running"  . (isset($jobname) ? " AND jobname = :jobname" : ""));
        $statement->execute(array('running' => true, "jobname" => $jobname));
        $rows = $statement->fetchAll();

        foreach ($rows as $row) {
            $rowtime = intval($row['start']);
            $diff = time() - $rowtime;
            if ($diff > $this->running_timeout) {
                $statement = $this->driver->prepare("UPDATE " . $this->table("job") . " SET running = :running, error = " . (strlen($row['error']) > 0 ? "CONCAT(error, '|' :error)" : ":error"). ", status = :status WHERE id = :id");
                $statement->execute(array('running' => false, "error" => "Timeout", "status" => "failure", "id" => $row['id']));
            } else {
                array_push($running, $row);
            }
        }

        return $running;
    }

    /**
     * Returns the array of table names
     * 
     * @return array The array of table names
     */
    public function tables() {
        return $this->tables;
    }

    /**
     * Returns a specific table name
     * 
     * @param string $name
     * @return string Name of table
     */
    public function table($name) {
        return $this->tables[$name];
    }

    /**
     * Returns the table prefix
     * 
     * @return string The table prefix
     */
    public function prefix() {
        return $this->table_prefix;
    }

    /**
     * Set all the needed table names 
     */
    private function setTableName() {
        $this->tables['job'] = $this->table_prefix . "job_log";
    }

    /**
     * Create all the needed tables in the database
     */
    private function createTables() {
        if (!isset($this->driver)) {
            Cronos::report("Cannot create tables: Missing database");
            return;
        }

        $sql = "CREATE TABLE IF NOT EXISTS `" . $this->table('job') . "` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `jobname` varchar(64) NOT NULL,
                    `running` tinyint(1) NOT NULL,
                    `start` int(11) NOT NULL,
                    `status` varchar(64) NOT NULL,
                    `run_time` float NOT NULL,
                    `log` text NOT NULL,
                    `error` text NOT NULL,
                    PRIMARY KEY (`id`)
                )";

        $this->driver->exec($sql);
    }

    /**
     * Destory the database connection
     */
    public function __destruct() {
        $this->driver = null;
    }

    /**
     * Return the database PDO driver
     * 
     * @return PDO Driver
     */
    public function driver() {
        return $this->driver;
    }

}

/**
 * CronosJob
 * Class representing a cron job
 */
class CronosJob {

    private $db;
    private $name = null;
    private $cron_expression = null;
    private $task = null;

    /**
     * The job is created with a name
     * 
     * @param string $name
     */
    public function __construct($name) {
        $this->name = $name;
    }

    /**
     * Set the database to allow logging
     * 
     * @param PDO $db
     */
    public function setDB($db) {
        $this->db = $db;
    }

    /**
     * Get the name
     * 
     * @return string
     */
    public function name() {
        return $this->name;
    }

    /**
     * Get the task for the job
     * 
     * @return mixed Job - Closure or path to script
     */
    public function getTask() {
        return $this->task;
    }

    /**
     * Get the CRON expression for the job
     * Yep, the one with stars and all that
     * 
     * @return string
     */
    public function getCronExp() {
        return $this->cron_expression;
    }

    /**
     * Set the CRON expression for the job
     * 
     * @param string $crontime
     * @return \CronosJob For chaining
     */
    public function at($cron_exp) {
        $this->cron_expression = $cron_exp;
        return $this;
    }

    /**
     * Set the task for the job
     * If a string is supplied, Cronos will assume that it is as path to php script
     * If a Closure (anon. func) is supplied, Cronos will call it when executed
     * 
     * @param mixed $task
     * @return \CronosJob For chaining
     */
    public function perform($task) {
        $this->task = $task;
        return $this;
    }

    /**
     * Shorthand for Cronos::register() for chaining
     * 
     * @return \CronosJob For chaining
     */
    public function register() {
        Cronos::register($this);
        return $this;
    }

    /**
     * Save the job as a line in the crontab
     * Not yet implemented
     */
    public function save() {
        // Save the job to the cron tab 
        // Not yet implemented
    }

    /**
     * Get the jobhandler for the job, see CronosJobHandler for details
     * 
     * @return \CronosJobHandler
     */
    public function handler() {
        $jobhandler = new CronosJobHandler($this->name, $this->db);
        return $jobhandler;
    }

}

/**
 * CronosJobHandler
 * 
 * Serves as an safe interface with the executed task
 */
class CronosJobHandler {

    private $db;
    private $recordid;
    private $jobname;
    private $status = null;
    private $statusText = array(
        "none",
        "success",
        "failure"
    );
    private $customStatus;
    private $logCount = 0;
    private $errorCount = 0;
    private $running = false;

    const log_delimiter = "|";

    /**
     * Create the handler as a shallow version of the job
     * with name and database
     * 
     * @param string $jobname
     * @param PDO $db
     * @param boolean $create
     */
    public function __construct($jobname, $db, $create = true) {
        $this->db = $db;
        $this->jobname = $jobname;
        $this->status = 0;

        if ($create) {
            $this->create();
        }
    }

    /**
     * Create an entry in the table
     * 
     * @return \CronosJobHandler
     */
    public function create() {
        if (!$this->running) {
            $data = array('jobname' => $this->jobname, "running" => true, "start" => time(), "status" => "starting");
            $STH = $this->db->driver()->prepare("INSERT INTO " . $this->db->table("job") . " (jobname, running, start, status) value (:jobname, :running, :start, :status)");
            $STH->execute($data);
            $this->recordid = $this->db->driver()->lastInsertId();
            $this->running = true;
            return $this;
        }
    }

    /**
     * Allow the task to tell if the job went successful
     */
    public function success() {
        if ($this->running) {
            $this->status = 1;
            $this->running = false;
        }
    }

    /**
     * Allow the task to tell if the job failed
     */
    public function failure() {
        if ($this->running) {
            $this->status = 2;
            $this->running = false;
        }
    }

    /**
     * Get the status integer
     * 
     * @return int
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Get the status text
     * none, success, failure, [custom], nonstatus etc.
     * 
     * @return string
     */
    public function getStatusText() {
        if (isset($this->statusText[$this->status])) {
            return $this->statusText[$this->status];
        } else if ($this->status === 3) {
            return $this->customStatus;
        } else {
            return "nostatus";
        }
    }

    /**
     * Update the status to a custom status
     * 
     * @param string $status
     */
    public function updateStatus($status) {
        if (!$this->running) {
            return;
        }

        $this->status = 3; // Custom status
        $this->customStatus = $status;

        $data = array('id' => $this->recordid, "status" => $status);
        $STH = $this->db->driver()->prepare("UPDATE " . $this->db->table("job") . " SET status = :status WHERE id = :id");
        $STH->execute($data);
    }

    /**
     * Add a message to the log field
     * 
     * @param string $msg
     */
    public function log($msg) {
        $data = array('id' => $this->recordid, "log" => $msg);
        $STH = $this->db->driver()->prepare("UPDATE " . $this->db->table("job") . " SET log = " . ($this->logCount > 0 ? "CONCAT(log, '" . self::log_delimiter . "' :log)" : ":log") . " WHERE id = :id");
        $STH->execute($data);
        $this->logCount++;
    }

    /**
     * Add an error message to the error field
     * 
     * @param string $msg
     * @param boolean $failure Report as failure?
     */
    public function error($msg, $failure = true) {
        $data = array('id' => $this->recordid, "error" => $msg);
        $STH = $this->db->driver()->prepare("UPDATE " . $this->db->table("job") . " SET error = " . ($this->errorCount > 0 ? "CONCAT(error, '" . self::log_delimiter . "' :error)" : ":error") . " WHERE id = :id");
        $STH->execute($data);
        $this->errorCount++;
        if ($failure) {
            $this->failure();
        }
    }

    /**
     * Close the job handler and update the database with the run time of the execution
     * 
     * @param float $running_time
     */
    public function close($running_time) {
        $status = $this->getStatusText();
        $data = array('id' => $this->recordid, "runtime" => $running_time, "status" => $status, "running" => false);
        $STH = $this->db->driver()->prepare("UPDATE " . $this->db->table("job") . " SET run_time = :runtime, status = :status, running = :running WHERE id = :id");
        $STH->execute($data);
        $this->running = false;
    }

}

/**
 * CronosExpression
 * 
 * Handles the evaluation of the cron expressions
 */
class CronosExpression {

    public static function match($expression, $time = null) {
        if ($time === null) {
            $time = time();
        }

        $cron_matches = array();

        $cron_regex = "/^(\s*)((.*?) ){4}(.*?)(\s*)$/";
        $cron_fields = array('minutes', 'hours', "day", "month", "dow");
        $field_to_date = array("minutes" => "i", "hours" => "H", "day" => "j", "month" => "n", "dow" => "w");

        $explode = explode(" ", $expression);
        if (!preg_match($cron_regex, $expression) || (count($cron_fields) !== count($explode))) {
            Cronos::report("Invalid cron expression: " . $expression);
        }

        $numbers = array_combine($cron_fields, $explode);

        foreach ($cron_fields as $cron_field) {
            $number = $numbers[$cron_field];
            if ($number === "*") {
                $cron_matches[$cron_field] = true;
            } else {
                $now_value = intval(date($field_to_date[$cron_field], $time));
                if (preg_match("/^\d+$/", $number, $matches)) {
                    $cron_value = intval($number);
                    if ($now_value == $cron_value) {
                        $cron_matches[$cron_field] = true;
                    } else {
                        $cron_matches[$cron_field] = false;
                    }
                } else if (preg_match("/^\*\/\d+$/", $number, $matches)) {
                    $split = explode("/", $number);
                    $cron_value = intval($split[1]);
                    if (($cron_value !== 0) && ($now_value % $cron_value == 0)) {
                        $cron_matches[$cron_field] = true;
                    } else {
                        $cron_matches[$cron_field] = false;
                    }
                } else if (preg_match("/^(\d+)(,\d+)*$/", $number, $matches)) {
                    $split = array_map('intval', explode(",", $number));
                    if (in_array($now_value, $split)) {
                        $cron_matches[$cron_field] = true;
                    } else {
                        $cron_matches[$cron_field] = false;
                    }
                } else if (preg_match("/^\d+\-\d+$/", $number, $matches)) {
                    list($low, $high) = array_map('intval', explode("-", $number));
                    if ($low <= $now_value && $now_value <= $high) {
                        $cron_matches[$cron_field] = true;
                    } else {
                        $cron_matches[$cron_field] = false;
                    }
                } else if (preg_match("/^\d+\-\d+\/\d+$/", $number, $matches)) {
                    list($low, $high, $interval) = array_map('intval', preg_split("/[\-\/]/", $number));
                    if (($interval !== 0) && ($now_value % $interval == $low % $interval) && ($low <= $now_value) && ($now_value <= $high)) {
                        $cron_matches[$cron_field] = true;
                    } else {
                        $cron_matches[$cron_field] = false;
                    }
                } else {
                    $cron_matches[$cron_field] = false;
                }
            }
        }

        return (!in_array(false, $cron_matches, true));
    }

}
